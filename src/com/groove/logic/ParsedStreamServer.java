package com.groove.logic;

import org.json.JSONException;
import org.json.JSONObject;

/*
	 {
		"header" : {
			"session" : "29a5d93368b0e884370e70002400848f",
			"serviceVersion" : "20100903",
			"prefetchEnabled" : true
		},
		
		"result" : {
			"FileID" : "19847168",
			"uSecs" : "217000000",
			"FileToken" : "2ajFwi",
			"ts" : 1362085434,
			"isMobile" : false,
			"SongID" : 1497716,
			"streamKey" : "64bb740fe2db0e407f4c31f33439a0e1df2cc371_512fcd42_16da74_12ed800_bab1ffeb_1_400",
			"Expires" : 1362087234,
			"streamServerID" : 1024,
			"ip" : "stream54-he.grooveshark.com"
		}
	}
 */

public class ParsedStreamServer {
	private String fileID;
	private String uSecs;
	private String fileToken;
	private String ts;
	private String songID;
	private String streamKey;
	private String expires;
	private String streamServerID;
	private String ip;
	
	private String responseString;
	
	public ParsedStreamServer(String contentJSON) {
		setResponseString(contentJSON);
		
		//System.out.println(contentJSON);
		
		try {
			JSONObject json = new JSONObject(contentJSON);
			//JSONObject header = (JSONObject) JSONSerializer.toJSON(json.get("header"));
			JSONObject result = new JSONObject(json.get("result").toString());
			
			fileID = result.getString("FileID");
			uSecs = result.getString("uSecs");
			fileToken = result.getString("FileToken");
			ts = result.getString("ts");
			songID = result.getString("SongID");
			streamKey = result.getString("streamKey");
			expires = result.getString("Expires");
			streamServerID = result.getString("streamServerID");
			ip = result.getString("ip");
		} catch (JSONException e) {
			System.out.println("heres the problem");
			System.out.println(contentJSON);
			e.printStackTrace();
			
			ip = "FAULT";
		}
		
	}
	
	public String getStreamKey() {
		return streamKey;
	}

	public void setStreamKey(String streamKey) {
		this.streamKey = streamKey;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getStreamServerID() {
		return streamServerID;
	}

	public void setStreamServerID(String streamServerID) {
		this.streamServerID = streamServerID;
	}

	public String getuSecs() {
		return uSecs;
	}

	public void setuSecs(String uSecs) {
		this.uSecs = uSecs;
	}

	public String getFileToken() {
		return fileToken;
	}

	public void setFileToken(String fileToken) {
		this.fileToken = fileToken;
	}

	public String getFileID() {
		return fileID;
	}

	public void setFileID(String fileID) {
		this.fileID = fileID;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public String getSongID() {
		return songID;
	}

	public void setSongID(String songID) {
		this.songID = songID;
	}

	public String getExpires() {
		return expires;
	}

	public void setExpires(String expires) {
		this.expires = expires;
	}

	public String getResponseString() {
		return responseString;
	}

	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}
}
