package com.groove.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class RequestHandler {
	
	public static byte[] fetchImage(String url) {
		byte[] imageBlob = null;
		
		try {
			URLConnection connection = new java.net.URL(url).openConnection();
			connection.setUseCaches(false);
            connection.setDoInput(true);
            			
			InputStream is = connection.getInputStream();
					    			
			byte[] buffer = new byte[4096];
		    int len;
		    int curLen = 0;
		    
		    imageBlob = new byte[connection.getContentLength()];
		    
		    while ((len = is.read(buffer)) > 0) {
		    	//Log.v("test", len + " => " + curLen + " => max " + imageBlob.length);
		        System.arraycopy(buffer, 0, imageBlob, curLen, len);
		    	curLen += len;
		    }
		    
		    return imageBlob;
		    
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void fetchSong(String serverIp, String key, String filename, Song song) {
		String content = "streamKey=" + key;
		
		try {
			URLConnection connection = new java.net.URL(serverIp + "/stream.php").openConnection();
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Length", "" + content.length());
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Cache-Control", "no-cache");
			
			OutputStream stream = connection.getOutputStream();
			stream.write(content.getBytes());
			stream.close();
			
			InputStream is = connection.getInputStream();

			OutputStream outstream = new FileOutputStream(new File(filename));
		    			
			byte[] buffer = new byte[4096];
		    int len;
		    
		    int totLen = 0;
		    
		    song.setTotalLength(connection.getContentLength());
		    
		    while ((len = is.read(buffer)) > 0) {
		    	totLen += len;
		    	song.setCurrentLength(totLen);
		        outstream.write(buffer, 0, len);
		    }
		    
		    outstream.close();
		    
		} catch (IOException e) {
			song.setCurrentLength(-2);
			song.setTotalLength(-2);
			
			e.printStackTrace();
		}
	}
	
	public static String send(String adress, String content) {
		try {
			URLConnection connection = new URL(adress).openConnection();
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Length", "" + content.length());
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Cache-Control", "no-cache");
			
			OutputStream stream = connection.getOutputStream();
				stream.write(content.getBytes());
			
			stream.close();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuffer sb = new StringBuffer();
			String str = br.readLine();
			
			while(str != null){
				sb.append(str);
				str = br.readLine();
			}
	
			br.close();
			return sb.toString();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "fail";
	}
	
}
