package com.groove.logic.HitParade;

public class HitParadeSong {
	private String demoUrl;
	private String songName;
	private String songArtist;
	
	public HitParadeSong(String demoUrl, String songName, String songArtist) {
		super();
		
		this.demoUrl = demoUrl;
		this.songName = songName;
		this.songArtist = songArtist;
	}

	public String getDemoUrl() {
		return demoUrl;
	}

	public String getSongName() {
		return songName;
	}

	public String getSongArtist() {
		return songArtist;
	}

	public void setDemoUrl(String demoUrl) {
		this.demoUrl = demoUrl;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public void setSongArtist(String songArtist) {
		this.songArtist = songArtist;
	}

}
