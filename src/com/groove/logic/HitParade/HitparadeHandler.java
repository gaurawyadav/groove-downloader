package com.groove.logic.HitParade;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class HitparadeHandler {
	public ArrayList<HitParadeSong> getTopList() {
		ArrayList<HitParadeSong> songList = new ArrayList<HitParadeSong>();
		
		try {
			HttpHandler myHandler = new HttpHandler();
			
			HttpGet myReq = new HttpGet("http://www.ultratop.be/nl/weekchart.asp?cat=s");
			HttpResponse response = myHandler.execute(myReq);
			
			// Get the response
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			
			String line;
			
			Pattern pName2 = Pattern.compile("^.*?class=\"navb\"><b>(.*?)</b> <img src.*?><br>(.*?)</a>.*?playAudio2\\('(.*?)'\\);.*?$");
			Pattern pName = Pattern.compile("^.*?class=\"navb\"><b>(.*?)</b><br>(.*?)</a>.*?playAudio2\\('(.*?)'\\);.*?$");
		
			int x = 1;
			while((line = rd.readLine()) != null) {
				String[] allLines = line.split("<a href=\"showitem.asp");
				
				for(int y = 0; y < allLines.length; y++) {
					
					Matcher mName = pName.matcher(allLines[y]);
					Matcher mName2 = pName2.matcher(allLines[y]);
					
					if(mName.matches()) {
						songList.add(new HitParadeSong(generateURL(mName.group(3)), mName.group(2), mName.group(3)));
						
						System.out.println(x + " Name is: " + mName.group(2) + " => "  + mName.group(3));
						generateURL(mName.group(3));
						x++;
					}
					
					if(mName2.matches()) {
						songList.add(new HitParadeSong(generateURL(mName2.group(3)), mName2.group(2), mName2.group(3)));
						
						System.out.println(x + " Name is: " + mName2.group(2) + " => "  + mName2.group(3));
						generateURL(mName2.group(3));
						x++;
					}
				}
				//System.out.println(line);
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return songList;
	}

	private static String generateURL(String id) {
		String base = "http://streamd.hitparade.ch/audio/";
		String first = "";
		
		first = id.substring(0, 3);
		first += "0000/";
		
		base += first + id + ".mp3";
		
		System.out.println(base);
		
		return base;
	}
}
