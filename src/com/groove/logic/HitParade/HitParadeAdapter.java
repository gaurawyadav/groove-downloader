package com.groove.logic.HitParade;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.groove.dlder.R;
import com.groove.dlder.SongAdapter;
import com.groove.logic.Song;

public class HitParadeAdapter extends ArrayAdapter<HitParadeSong>{
	
	public HitParadeAdapter(Context context, int resource, int textViewResourceId,
			List<HitParadeSong> items) {
		super(context, resource, textViewResourceId, items);
	}

	private class Viewholder {
		TextView artist, titel, album;
		CheckBox chbxSelectedForDownload;
	}

	private static Viewholder viewholder;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			// inflate the custom layout
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.song_item, null);

			viewholder = new Viewholder();
			
			// cache the views
			viewholder.artist = (TextView) convertView.findViewById(R.id.txtListItemArtist);
			viewholder.titel = (TextView) convertView.findViewById(R.id.txtListItemTitel);
			viewholder.album = (TextView) convertView.findViewById(R.id.txtListItemAlbum);
			viewholder.chbxSelectedForDownload = (CheckBox) convertView.findViewById(R.id.chbxSelectForDownload);
			
			// link the cached views to the convertview
			convertView.setTag(viewholder);

		} else {
			viewholder = (Viewholder) convertView.getTag();
		}
		
		// set the data to be displayed
		//((another.getTrackNum() < 10 ? 0 + another.getTrackNum() : another.getTrackNum())
		if(!this.getItem(position).isAlbum()) {
			viewholder.artist.setText(this.getItem(position).getArtistName());
			
			if(this.getItem(position).isAlbumSong())
				viewholder.titel.setText((this.getItem(position).getTrackNum() < 10 ? "" + 0 + this.getItem(position).getTrackNum() : this.getItem(position).getTrackNum()) + " - " + this.getItem(position).getSongName());
			else
				viewholder.titel.setText(this.getItem(position).getSongName());
			
			viewholder.album.setText(this.getItem(position).getAlbumName());
			viewholder.chbxSelectedForDownload.setChecked(checkStatus.get(position));
		} else {
			viewholder.artist.setText(this.getItem(position).getArtistName());
			viewholder.titel.setText(this.getItem(position).getAlbumName());
			viewholder.album.setText(this.getItem(position).getSongName());
			viewholder.chbxSelectedForDownload.setChecked(checkStatus.get(position));
		}

		// return the view to be displayed
		return convertView;
	}
	
	public boolean getCheckStatus(int position) {
		return checkStatus.get(position);
	}
	
	public void setCheckStatus(int position, boolean checkStatus) {
		SongAdapter.checkStatus.set(position, checkStatus);
	}
	
	@Override
	public void add(Song object) {
		super.add(object);
		SongAdapter.checkStatus.add(false);
	}

	@Override
	public void clear() {
		super.clear();
		SongAdapter.checkStatus = new ArrayList<Boolean>();
	}
}

