package com.groove.logic;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Utils {
	public static String generateRandomUUID() {
		String uuid = "";
		
		for(int i = 0; i < 8; i++)
			uuid += Utils.generateHexChar();
		
		uuid += '-';
		
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 4; j++)
				uuid += Utils.generateHexChar();
			
			uuid += '-';
		}
		
		uuid += Utils.generateRandomSalt() + Utils.generateRandomSalt();
		
		return uuid.toUpperCase();
	}
	
	public static String generateRandomSalt() {
		String salt = "";
		
		for(int i = 0; i < 6; i++)
			salt += generateHexChar();
			
		return salt;
	}
	
	public static String generateHexChar() {
		Random rand = new Random();
		int myRandomNumber = rand.nextInt(15) + 1; // Generates a random number between 0x10 and 0x20
		
		return Integer.toHexString(myRandomNumber);
	}

	public static String encodeMD5(String toEncrypt) {
		return encode(toEncrypt, "MD5");
	}
	
	public static String encodeSha1(String toEncrypt) {
		return encode(toEncrypt, "SHA-1");
	}
	
	public static String encode(String toEncrypt, String type) {
		byte[] msg;
		
		try {
			msg = toEncrypt.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance(type);
			md.update(msg);

			byte byteData[] = md.digest();

			StringBuffer sb2 = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb2.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			
			toEncrypt = sb2.toString();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}	
		
		return toEncrypt;
	}
}
