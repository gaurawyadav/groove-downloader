package com.groove.logic;

import android.os.Parcel;
import android.os.Parcelable;

public class GrooveSession implements Parcelable{
	private String SessionID;
	private String SessionKey;
	private String SessionUUID;
	
	private String streamClient = "jsqueue";
	private String streamClientRev = "20120830";
	
	private String tokenClient = "htmlshark";
	private String tokenClientRev = "20120830";
	
	private String countryJSON = "{\"CC4\":0,\"CC1\":2097152,\"ID\":22,\"IPR\":0,\"CC3\":0,\"DMA\":0,\"CC2\":0}";

	public GrooveSession() {
		//Generate a random sessionID
		SessionID = Utils.encodeMD5(Utils.generateRandomSalt() + Utils.generateRandomSalt() + 4);
		
		//Session key is needed to fetch the token & is md5 hash of your sessionID
		SessionKey = Utils.encodeMD5(SessionID);
		
		//Session UUID, no idea what it does, just randomly generated
		SessionUUID = Utils.generateRandomUUID();
	}
	
	public GrooveSession(Parcel in) {
		SessionID = in.readString();
		SessionKey = in.readString();
		SessionUUID = in.readString();
		
		streamClient = in.readString();
		streamClientRev = in.readString();
		
		tokenClient = in.readString();
		tokenClientRev = in.readString();
		
		countryJSON = in.readString();
	}

	public String getSessionKey() {
		return SessionKey;
	}

	public void setSessionKey(String sessionKey) {
		SessionKey = sessionKey;
	}

	public String getSessionID() {
		return SessionID;
	}

	public void setSessionID(String sessionID) {
		SessionID = sessionID;
	}

	public String getStreamClient() {
		return streamClient;
	}

	public void setStreamClient(String streamClient) {
		this.streamClient = streamClient;
	}

	public String getStreamClientRev() {
		return streamClientRev;
	}

	public void setStreamClientRev(String streamClientRev) {
		this.streamClientRev = streamClientRev;
	}

	public String getTokenClient() {
		return tokenClient;
	}

	public void setTokenClient(String tokenClient) {
		this.tokenClient = tokenClient;
	}

	public String getTokenClientRev() {
		return tokenClientRev;
	}

	public void setTokenClientRev(String tokenClientRev) {
		this.tokenClientRev = tokenClientRev;
	}

	public String getCountryJSON() {
		return countryJSON;
	}

	public void setCountryJSON(String countryJSON) {
		this.countryJSON = countryJSON;
	}

	public String getSessionUUID() {
		return SessionUUID;
	}

	public void setSessionUUID(String sessionUUID) {
		SessionUUID = sessionUUID;
	}
	
	public static final Parcelable.Creator<GrooveSession> CREATOR = new Parcelable.Creator<GrooveSession>() {
		public GrooveSession createFromParcel(Parcel in) {
			return new GrooveSession(in);
		}
		
		public GrooveSession[] newArray(int size) {
			return new GrooveSession[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(SessionID);
		out.writeString(SessionKey);
		out.writeString(SessionUUID);
		out.writeString(streamClient);
		out.writeString(streamClientRev);
		out.writeString(tokenClient);
		out.writeString(tokenClientRev);
		out.writeString(countryJSON);
	}
}
