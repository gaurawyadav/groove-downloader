package com.groove.logic;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.groove.dlder.Settings;

public class GroovePopular {
	private String session;
	private String serviceVersion;
	private boolean prefetchEnabled;
	
	private ArrayList<Song> songList;
		
	private String contentJSON;
	
	public GroovePopular(GrooveToken myToken, GrooveSession mySession) {		
		contentJSON = RequestHandler.send("http://cowbell.grooveshark.com/more.php?popularGetSongs",
			RequestCreator.generatePopularSongRequest(myToken, mySession));
		
		try {
			JSONObject json = new JSONObject(contentJSON);
			JSONObject header = new JSONObject(json.get("header").toString());
			
			JSONObject result = new JSONObject(json.get("result").toString());
			
			session = header.getString("session");
			serviceVersion = header.getString("serviceVersion");
			prefetchEnabled = header.getBoolean("prefetchEnabled");
			
			songList = new ArrayList<Song>();
			
			JSONArray unparsedList = result.getJSONArray("Songs");
			
			int maxCount;
						
			if(unparsedList.length() > Settings.getShowSongCount())
				maxCount = Settings.getShowSongCount();
			else
				maxCount = unparsedList.length();
						
			for(int i = 0; i < maxCount; i++) {
				JSONObject thisSong = (JSONObject) unparsedList.get(i);
				
				Song me = new Song(thisSong, false, false, true);
				
				songList.add(me);
			}
		} catch (JSONException e) {
			System.out.println("Banned in groovepopular");
			e.printStackTrace();
		}
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getServiceVersion() {
		return serviceVersion;
	}

	public void setServiceVersion(String serviceVersion) {
		this.serviceVersion = serviceVersion;
	}

	public boolean isPrefetchEnabled() {
		return prefetchEnabled;
	}

	public void setPrefetchEnabled(boolean prefetchEnabled) {
		this.prefetchEnabled = prefetchEnabled;
	}

	public ArrayList<Song> getSongList() {
		return songList;
	}

	public void setSongList(ArrayList<Song> songList) {
		this.songList = songList;
	}
	
	public String getContentJSON() {
		return contentJSON;
	}

	public void setContentJSON(String contentJSON) {
		this.contentJSON = contentJSON;
	}
}
