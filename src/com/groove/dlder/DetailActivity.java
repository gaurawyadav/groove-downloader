package com.groove.dlder;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.groove.logic.RequestHandler;
import com.groove.logic.Song;

public class DetailActivity extends Activity {
	
	private Song song;
	
	private Button btnDownload;
	private TextView tvArtist;
	private TextView tvTitel;
	private TextView tvAlbum;
	private TextView tvDuration;
	private TextView lblDuration;
	private ImageView imgvSongpicture;
	
	private Notification myNotification;
	private NotificationManager mNotificationManager;
	private RemoteViews mRemoteView;
	private static int gId = 0;
	private int mId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		
		Intent received = getIntent();
		
		song = received.getParcelableExtra("song");
		
		btnDownload = (Button) findViewById(R.id.btnDownload);
		tvTitel = (TextView) findViewById(R.id.txtDTitel);
		tvArtist = (TextView) findViewById(R.id.txtDArtist);
		tvAlbum = (TextView) findViewById(R.id.txtDAlbum);
		tvDuration = (TextView) findViewById(R.id.fieldDuration);
		lblDuration = (TextView) findViewById(R.id.lblDuration);
		imgvSongpicture = (ImageView) findViewById(R.id.imgvSongPicture);
		
		tvTitel.setText(song.getSongName());
		tvArtist.setText(song.getArtistName());
		tvAlbum.setText(song.getAlbumName());
		tvDuration.setText(song.getEstimateDuration());
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				byte[] byteArray = RequestHandler.fetchImage("http://images.gs-cdn.net/static/albums/120_" + song.getCovertArtFilename());
				final Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
				
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						imgvSongpicture.setImageBitmap(bmp);
					}
				});
			}
		}).start();
		
		if(((int) Double.parseDouble(song.getEstimateDuration())) > 0) {
			tvDuration.setText("" + Double.parseDouble(song.getEstimateDuration())/60);
		} else {
			lblDuration.setVisibility(View.INVISIBLE);
			tvDuration.setVisibility(View.INVISIBLE);
		}
		
		btnDownload.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getBaseContext(), "Downloading " + song.getSaveName(), Toast.LENGTH_LONG).show();
				
				new Thread(new Runnable() {
					@Override
					public void run() {
						
						song.download(Settings.getLocation() + song.getSaveName());
						
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(getBaseContext(), "Downloaded " + song.getSaveName(), Toast.LENGTH_LONG).show();
								
								mRemoteView.setTextViewText(R.id.txtNotificationTitle, "Downloaded");
								mRemoteView.setProgressBar(R.id.pbNotificationProgress, 100, 100, false);
								myNotification.flags = Notification.FLAG_AUTO_CANCEL;
								mNotificationManager.notify(mId, myNotification);
							}
						});
					}
				}).start();
				
				new Thread(new Runnable() {

					@Override
					public void run() {
						myNotification = new Notification();
						myNotification.icon = R.drawable.ic_launcher;

						mRemoteView = new RemoteViews(getPackageName(), R.layout.song_notification);
						
						mRemoteView.setImageViewResource(R.id.ivNotificationImage, R.drawable.ic_launcher);
						mRemoteView.setTextViewText(R.id.txtNotificationTitle, "Downloading");
						mRemoteView.setTextViewText(R.id.txtNotificationText, song.getArtistName() + " - " + song.getSongName());
						mRemoteView.setProgressBar(R.id.pbNotificationProgress, 100, 0, false);
						
						myNotification.contentView = mRemoteView;
						
						Intent intent = new Intent();
						PendingIntent contentIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, 0);
						
						myNotification.contentIntent = contentIntent;
						
						mNotificationManager.notify(mId , myNotification);
						
						while(song.getTotalLength() == -1 && song.getCurrentLength() == -1) {
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						
						while(song.getTotalLength() != song.getCurrentLength()) {
							double current = song.getCurrentLength()/(double) song.getTotalLength()*100;
														
							mRemoteView.setProgressBar(R.id.pbNotificationProgress, 100, (int) current, false);
							mNotificationManager.notify(mId, myNotification);
							
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						
						mRemoteView.setTextViewText(R.id.txtNotificationTitle, "Downloaded");
						mNotificationManager.notify(mId, myNotification);
					}
				}).start();
			}
		});
		
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mId = gId;
		gId++;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detail, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intent = new Intent(getBaseContext(), PrefsActivity.class);
			startActivity(intent);
		}

		return super.onOptionsItemSelected(item);
	}

}
