package com.groove.dlder;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import com.groove.logic.Song;

public class DownloadQueueManager {
	private static volatile ArrayList<Song> downloadList = null;
	private static volatile boolean isRunning = false;
	private static volatile Thread myDownloads = null;

	private static int songCount = 0;

	// Notification stuffz
	private static int gId = 0;

	public static void download(final HashSet<Song> songs) {
		if(downloadList == null)
			downloadList = new ArrayList<Song>();
		
		if(songs.size() == 0)
			return;
		
		//This means its an album, it's really a hackaround 
		//	and will be fixed if i find spare time (No)
		if(songs.size() == 1 && ((Song) songs.toArray()[0]).getSongID() == -1337) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					((Song) songs.toArray()[0]).download("");
				}
			}).start();
			
			return;
		} else {
			downloadList.addAll(new ArrayList<Song>(songs));
			setSongCount(getSongCount() + songs.size());

			if(!isRunning) {
				isRunning = true;
				
				setMyDownloads(new Thread(new Runnable() {
					@Override
					public void run() {
						downloadAll();
						setSongCount(0);
						isRunning = false;
					}
				}));
				myDownloads.start();
			}
		}
	}

	
	private static void downloadAll() {
		Notification myNotification;
		RemoteViews mRemoteView;

		int curCount = 0;
		
		while(downloadList.size() > 0) {
			
			int mId = gId;
			gId++;
			
			myNotification = new Notification();
			myNotification.icon = R.drawable.icon;
			
			final Song me = downloadList.remove(0);
						
			mRemoteView = new RemoteViews(MainActivity.getMyPackageName(), R.layout.song_notification);
			
			mRemoteView.setImageViewResource(R.id.ivNotificationImage, R.drawable.icon);
			mRemoteView.setTextViewText(R.id.txtNotificationTitle, "Downloading");
			mRemoteView.setTextViewText(R.id.txtNotificationText, me.getArtistName() + " - " + me.getSongName());
			mRemoteView.setProgressBar(R.id.pbNotificationProgress, 100, 0, false);
			
			myNotification.contentView = mRemoteView;
			
			String saveLocation = "";
			
			if(me.isAlbumSong()) {
				File location = new File(Settings.getLocation() + me.getAlbumName() + "/");
				location.mkdirs();
				
				saveLocation = Settings.getLocation() + me.getAlbumName() + "/" + 
						(me.getTrackNum() < 10 ? "0" + me.getTrackNum() : me.getTrackNum()) + 
						" - " + me.getSaveName();
			}
			else
				saveLocation = Settings.getLocation() + me.getSaveName();
						
			final File file = new File(saveLocation);
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(file), "audio/mp3");
						
			myNotification.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
			myNotification.contentIntent = PendingIntent.getActivity(MainActivity.getMyBaseContext(), 0, new Intent(), 0);
			
			MainActivity.getNotificationManager().notify(mId , myNotification);
			
			new Thread(new Runnable() {
				@Override
				public void run() {
					System.out.println("dling => " + Uri.fromFile(file).getPath());
					me.download(Uri.fromFile(file).getPath());
				}
			}).start();
			
			while(me.getTotalLength() == -1 && me.getCurrentLength() == -1) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
						
			while(me.getTotalLength() != me.getCurrentLength()) {
				double current = me.getCurrentLength()/(double) me.getTotalLength()*100;
								
				mRemoteView.setProgressBar(R.id.pbNotificationProgress, 100, (int) current, false);
				MainActivity.getNotificationManager().notify(mId, myNotification);
				
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			MainActivity.getNotificationManager().cancel(mId);
			
			myNotification = new Notification();
			myNotification.icon = R.drawable.icon;
						
			mRemoteView = new RemoteViews(MainActivity.getMyPackageName(), R.layout.song_notification);
			mRemoteView.setImageViewResource(R.id.ivNotificationImage, R.drawable.icon);
			mRemoteView.setTextViewText(R.id.txtNotificationTitle, "Downloaded");
			mRemoteView.setTextViewText(R.id.txtNotificationText, me.getArtistName() + " - " + me.getSongName());
			mRemoteView.setProgressBar(R.id.pbNotificationProgress, 100, 100, false);
			
			myNotification.contentView = mRemoteView;
			
			PendingIntent contentIntent = PendingIntent.getActivity(MainActivity.getMyBaseContext(), 0, intent, 0);
			myNotification.contentIntent = contentIntent;
			myNotification.flags = Notification.FLAG_AUTO_CANCEL;
			
			MainActivity.getNotificationManager().notify(mId, myNotification);
			
			if(curCount > 5) {
				try {
					Thread.sleep(Settings.getThrottle());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}			
		}
		
		myDownloads = null;
	}


	public static Thread getMyDownloads() {
		return myDownloads;
	}


	public static void setMyDownloads(Thread myDownloads) {
		DownloadQueueManager.myDownloads = myDownloads;
	}


	public static int getSongCount() {
		return songCount;
	}


	public static void setSongCount(int songCount) {
		DownloadQueueManager.songCount = songCount;
	}
}
