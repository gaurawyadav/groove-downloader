package com.groove.dlder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.groove.logic.Song;

public class AlbumDetailActivity extends Activity {
	
	private Button btnDownloadAlbum;
	private Button btnDownloadSelected;
	
	private TextView albumName;
	private TextView albumArtist;
	
	private Song currentSong;
	
	private Song album;
	
	private ListView lvSongList;
	private SongAdapter songAdapter;
	//TODO doubt that this one needs to be static
	//the static was ONLY to not erase the list if you got out of the screen
	//(e.g. going to a detail screen, exiting the app with home button, stuff like that)
	private volatile static ArrayList<Song> songs;
	private volatile static HashSet<Song> toDownload;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_album_detail);
		
		Intent received = getIntent();
		
		//TODO would rename this to album, and possibly a data member
		album = received.getParcelableExtra("song");
		
		btnDownloadAlbum = (Button) findViewById(R.id.btnDldAlbum);
		btnDownloadSelected= (Button) findViewById(R.id.btnDldSelected);
		
		albumName = (TextView) findViewById(R.id.txtADAlbum);
		albumArtist = (TextView) findViewById(R.id.txtADArtist);
		
		albumName.setText(album.getAlbumName());
		albumArtist.setText(album.getArtistName());
		
		// initialising the lists if necessary
		if (songs == null)
			songs = new ArrayList<Song>();
		
		if (toDownload == null)
			toDownload = new HashSet<Song>();
		
		//TODO you were trying to fill the adapter without making one first
		//(this code was underneath the thread
		lvSongList = (ListView)	findViewById(R.id.lvHitParadeSong);
		
		// making new adapter and putting it on the listview
		songAdapter = new SongAdapter(this, R.layout.song_item,
				R.id.txtListItemArtist, songs);
		lvSongList.setAdapter(songAdapter);
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				songs = new ArrayList<Song>(album.getAllSongsInAlbum());
				Collections.sort(songs);

				runOnUiThread(new Runnable() {
					public void run() {
						songAdapter.clear();
						
						for (Song me : songs)
							songAdapter.add(me);
				}});

			}
		}).start();
		
		/**Hooks & Generation of variables**/
		btnDownloadAlbum.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				HashSet<Song> hack = new HashSet<Song>();
				hack.add(album);
				
				Toast.makeText(getBaseContext(), "Downloading Album", 
						Toast.LENGTH_LONG).show();
				
				DownloadQueueManager.download(hack);
			}
		});
		
		btnDownloadSelected.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getBaseContext(), "Downloading Selected Songs", 
						Toast.LENGTH_LONG).show();
				
				DownloadQueueManager.download(toDownload);

				clearDownloadsChecked();
			}
		});
		
		lvSongList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Song me = songAdapter.getItem(position);

				boolean checkedStatus = songAdapter.getCheckStatus(position);

				if (checkedStatus) {
					if (toDownload.contains(me))
						toDownload.remove(me);

					songAdapter.setCheckStatus(position, false);
				} else {
					songAdapter.setCheckStatus(position, true);
					toDownload.add(me);
				}

				lvSongList.invalidateViews();
			}

		});
		
		registerForContextMenu(lvSongList);
		lvSongList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {
						// this sets the current song to the song which is long
						// clicked
						currentSong = songAdapter.getItem(position);

						// this means the longclick isn't consumed and the
						// contextmenu
						// gets called
						return false;
					}

				});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		currentSong = null;
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.download:
				Toast.makeText(getBaseContext(), "Downloading Song", 
						Toast.LENGTH_LONG).show();
				
				HashSet<Song> hack = new HashSet<Song>();
				hack.add(currentSong);
				
				DownloadQueueManager.download(hack);
				
				clearDownloadsChecked();
				return true;
				
			case R.id.details:
				Intent intent = new Intent(getBaseContext(), DetailActivity.class);
				intent.putExtra("song", currentSong);
	
				startActivity(intent);
				return true;
			
			default:
				return super.onContextItemSelected(item);

		}
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.album_detail, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intent = new Intent(getBaseContext(), PrefsActivity.class);
			startActivity(intent);
		}

		return super.onOptionsItemSelected(item);
	}
	
	//TODO make this into utility?
	protected void clearDownloadsChecked() {
		for (int x = 0; x < songs.size(); x++) {
			songAdapter.setCheckStatus(x, false);
		}

		toDownload = new HashSet<Song>();

		lvSongList.invalidateViews();
	}

}
